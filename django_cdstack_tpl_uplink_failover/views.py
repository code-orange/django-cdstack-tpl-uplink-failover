from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = (
        "django_cdstack_tpl_uplink_failover/django_cdstack_tpl_uplink_failover"
    )

    generate_config_static(zipfile_handler, template_opts, module_prefix)

    return True
